package com.example.onlineLibrary;

import com.example.onlineLibrary.persistence.model.Book;
import com.example.onlineLibrary.persistence.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.NoSuchElementException;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/books")
public class OnlineLibraryController {
    private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    BookService bookService;

    @GetMapping("")
    public List<Book> list() {
        return bookService.listAllBooks();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Book> get(@PathVariable Integer id) {
        try {
            Book book = bookService.getBook(id);
            return new ResponseEntity<Book>(book, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(path = "/", method = POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public void add(Book book, @RequestParam("file") MultipartFile file) throws IOException {
        book.setBookContent(file.getBytes());
        bookService.saveBook(book);
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        bookService.deleteBook(id);
    }

}