package com.example.onlineLibrary.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

@Data
public class BookDto {
    private Long bookId;
    private String bookName;
    private LocalDate dateOfPublication;
    private String authorName;
    private String genreName;
    private String publisherName;
    private MultipartFile bookContent;
    private String shortDescription;
    private int nrOfPages;
}