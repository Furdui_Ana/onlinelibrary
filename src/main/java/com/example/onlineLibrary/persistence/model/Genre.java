package com.example.onlineLibrary.persistence.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "GENRE")
@Data
public class Genre {
    @Id
    @Column(name = "GENRE_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long genreId;

    @Column(name = "GENRE_NAME")
    private String genreName;

    @Column(name = "GENRE_DESCRIPTION")
    private String genreDescription;
}