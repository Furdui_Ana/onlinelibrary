package com.example.onlineLibrary.persistence.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "PUBLISHER")
@Data
public class Publisher {

    @Id
    @Column(name = "PUBLISHER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long publisherId;

    @Column(name = "PUBLISHER_NAME")
    private String publisherName;

}