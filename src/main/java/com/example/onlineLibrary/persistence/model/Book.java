package com.example.onlineLibrary.persistence.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "BOOK")
@Data
public class Book {
    @Id
    @Column(name = "BOOK_ID")
    @GeneratedValue (strategy = GenerationType.SEQUENCE)
    private Long bookId;

    @Column(name = "BOOK_NAME")
    private String bookName;

    @Column(name = "DATE_OF_PUBLICATION")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfPublication;

    @Column(name = "AUTHOR_NAME")
    private String authorName;

    @Column(name = "GENRE_NAME")
    private String genreName;

    @Column(name = "PUBLISHER_NAME")
    private String publisherName;
    @Lob
    @Column(name = "BOOK_CONTENT")
    private byte[] bookContent;

    @Column(name = "NUMBER_OF_PAGES")
    private int nrOfPages;

    @Column(name = "SHORT_DESCRIPTION")
    private String shortDescription;
}
