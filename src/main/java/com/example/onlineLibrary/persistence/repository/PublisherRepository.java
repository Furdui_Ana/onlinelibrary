package com.example.onlineLibrary.persistence.repository;

import com.example.onlineLibrary.persistence.model.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PublisherRepository extends JpaRepository<Publisher, Long> {
}
