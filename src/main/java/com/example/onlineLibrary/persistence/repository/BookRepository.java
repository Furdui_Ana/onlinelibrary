package com.example.onlineLibrary.persistence.repository;


import com.example.onlineLibrary.dto.BookDto;
import com.example.onlineLibrary.persistence.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {

}