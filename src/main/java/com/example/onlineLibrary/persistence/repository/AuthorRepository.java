package com.example.onlineLibrary.persistence.repository;

import com.example.onlineLibrary.persistence.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
