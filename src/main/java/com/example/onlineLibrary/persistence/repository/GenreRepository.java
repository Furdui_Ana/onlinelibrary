package com.example.onlineLibrary.persistence.repository;

import com.example.onlineLibrary.persistence.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, Long> {
}
