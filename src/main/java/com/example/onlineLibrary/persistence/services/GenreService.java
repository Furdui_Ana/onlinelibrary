package com.example.onlineLibrary.persistence.services;

import com.example.onlineLibrary.persistence.model.Book;
import com.example.onlineLibrary.persistence.model.Genre;
import com.example.onlineLibrary.persistence.repository.BookRepository;
import com.example.onlineLibrary.persistence.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GenreService {

    @Autowired
    private GenreRepository genreRepository;

    public void saveGenre(Genre genre) {
        genreRepository.save(genre);
    }
}
