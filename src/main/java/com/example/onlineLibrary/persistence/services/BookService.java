package com.example.onlineLibrary.persistence.services;

import com.example.onlineLibrary.persistence.model.Book;
import com.example.onlineLibrary.persistence.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    public List<Book> listAllBooks() {
        return bookRepository.findAll();
    }

    public void saveBook(Book book) {
        bookRepository.save(book);
    }

    public Book getBook(long id) {
        return bookRepository.findById(id).get();
    }

    public void deleteBook(long id) {
        bookRepository.deleteById(id);
    }
}
