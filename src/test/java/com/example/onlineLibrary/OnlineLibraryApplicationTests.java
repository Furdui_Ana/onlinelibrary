package com.example.onlineLibrary;

import com.example.onlineLibrary.config.DBUnitConfig;
import com.example.onlineLibrary.persistence.model.Genre;
import com.example.onlineLibrary.persistence.services.GenreService;
import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

class OnlineLibraryApplicationTests extends DBUnitConfig {

	private GenreService service = new GenreService();
	private EntityManager em = Persistence.createEntityManagerFactory("DBUnitEx").createEntityManager();

	public OnlineLibraryApplicationTests(String name) {
		super(name);
	}

	@Test
	public void testSave() throws Exception {
		Genre genre = new Genre();
		genre.setGenreName("comedy");
		genre.setGenreDescription("Vernugora");
		service.saveGenre(genre);
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(
				Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("genre-data.xml"));
		IDataSet actualData = tester.getConnection().createDataSet();
		String[] ignore = {"id"};
		Assertion.assertEqualsIgnoreCols(expectedData, actualData, "genre", ignore);
	}

}